# Laboratoires

Ce dépôt contient les énoncés des laboratoires du cours [INF1070 Utilisation et
administration des systèmes
informatiques](https://etudier.uqam.ca/cours?sigle=INF1070). Il peut être
consulté directement en ligne. Il est aussi recommandé de le cloner localement
sur votre machine si vous souhaitez récupérer le code source des énoncés ou
encore les fichiers qui sont fournis en soutien aux laboratoires.

## Installer Git

Vous devez d'abord vous assurer que le logiciel Git est bien installé sur votre
machine. Dans les laboratoires, il est déjà installé. Si vous êtes sur un
système Debian/Ubuntu personnel et que vous avez les droits d'installation, il
suffit d'entrer la commande

```sh
sudo apt-get install git-core
```

pour l'installation.

## Cloner ce dépôt

Ensuite, pour cloner le dépôt, ouvrez un terminal,
positionnez-vous dans le répertoire de votre choix et lancez la commande

```sh
git clone https://gitlab.com/ablondin/inf1070-labos.git
```

pour copier le dépôt. Vous avez ensuite un dossier nommé `inf1070-labos` que
vous pouvez explorer comme n'importe quel dossier.

## Énoncés des laboratoires

Pour accéder aux énoncés des laboratoires, il suffit de cliquer sur les liens
suivants:

- [Labo 1](labo01/README.md)
- [Labo 2](labo02/README.md)
- [Labo 3](labo03/README.md)
- [Labo 4](labo04/README.md)
- [Labo 5](labo05/README.md)
- [Labo 6](labo06/README.md)
- [Labo 7](labo07/README.md)
- [Labo 8](labo08/README.md)
- [Labo 9](labo09/README.md)
- [Labo 10](labo10/README.md)
- [Labo 11](labo11/README.md)
- [Labo 12](labo12/README.md)

## Exercices facultatifs

### Vim

- [Macros](vim/macros/README.md)
- [Interface système](vim/interface-systeme/README.md)

