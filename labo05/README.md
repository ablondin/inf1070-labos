# Laboratoire 5 : Chercher, filtrer couper

## Exercice 1 : Dictionnaires de mots (30 mins)

### 1 : Récupérer une liste de mots

Consultez le répertoire `/usr/share/dict/`. Il devrait contenir différents
fichiers textes contenant des dictionnaires. Pour cet exercice, nous utiliserons
le dictionnaire contenu dans le fichier `/usr/share/dict/words`, mais s'il n'est
pas disponible sur votre machine, vous pouvez choisir n'importe lequel.

La commande `grep` vous sera utile ici. En particulier, deux caractères spéciaux
permettent d'identifier le début d'une ligne (caractère chapeau `^`) et la fin
d'une ligne (caractère `$`).

### 2 : Questions

Répondez en particulier aux questions suivantes:

1. Combien de mots contient le dictionnaire?
2. Combien de mots contiennent la chaîne `with`?
3. Combien de mots commencent par la chaîne `with`?
4. Combien de mots terminent par la chaîne `with`?
5. Combien de mots terminent par `'s`?

Ensuite, à l'aide de la commande `column` effectuez les actions suivantes:

6. Placez tous les mots qui commencent par la lettre majuscule `T` et placez-les
   dans un fichier nommé `T.txt` en les répartissant sur 4 colonnes.
7. Placez tous les noms propres (mots qui commencent par une majuscule) et
   placez-les dans un fichier nommé `noms-propres.txt`. Combien y en a-t-il?

## Exercice 2 : Base de données textuelle (60 mins)

[GeoNames](https://www.geonames.org/) est un projet qui fournit gratuitement
différentes bases de données sous format textuel sur des lieux géographiques un
peu partout sur la planète.

Pour cette question, voici quelques commandes qui pourraient vous être utiles.
N'hésitez pas à consulter leur manuel pour mieux connaître les options:

- `wget`, pour télécharger un fichier
- `wc`, pour compter le nombre de lignes, caractères, etc. d'un fichier
- `cut`, pour extraire une partie de ligne
- `grep`, pour sélectionner les lignes contenant un certain motif
- `sort`, pour trier des lignes selon certains critères
- `uniq`, pour supprimer les lignes consécutives identiques
- `head`, pour récupérer les première lignes d'un fichier
- `tail`, pour récupérer les dernières lignes d'un fichier

### 1 : Récupérer les fichiers

Rendez-vous sur la [page qui contient les
données](http://download.geonames.org/export/dump/).

À l'aide d'une commande, téléchargez en ligne de commande les fichiers
`readme.txt`, `countryInfo.txt` et `CA.zip`.

Consultez le fichier `readme.txt` afin de vous familiariser avec la structure et
le contenu des autres fichiers.

Consultez ensuite le fichier `countryInfo.txt` pour étudier son contenu.

Toujours en ligne de commande, décompressez le fichier `CA.zip` et ouvrez-le
pour vous familiariser avec son contenu. Notez sa taille et son nombre de
lignes.

### 2 : Nettoyage des fichiers

Comme les fichiers `countryInfo.txt` et `CA.txt` contiennent des champs vides,
il convient d'abord de les nettoyer en insérant des valeurs `N/A`.

Pour cela, utilisez le script exécutable [`clean`](./clean) et placez les
résultats dans les fichiers `country-clean.txt` et `CA-clean.txt`.

### 3 : Supprimer les commentaires

Entrez la commande

```sh
grep -v '^#' countryInfo.txt
```

Celle-ci utilise l'expression régulière `^#` qui correspond à toutes les lignes
qui commencent par le caractère `#`. En particulier, le caractère `^` indique le
début d'une ligne.

En lisant le manuel, vérifiez le rôle de l'option `-v`.

La commande qui utilise `grep` ci-haut vous sera utile pour la suite lorsque
vous ferez des requêtes sur les différents fichiers afin d'éliminer les lignes
qui contiennent des commentaires. N'hésitez pas à sauvegarder, si vous le
souhaitez, une version "nettoyée" de chacun des fichiers à l'aide d'une
redirection `>`.

### 4 : Requêtes sur des pays

En utilisant le moins de commandes possibles, répondez aux questions suivantes.

1. Quels sont les codes de 3 lettres des 10 pays dont la population est la plus
   importante?
2. Quels sont les codes de 3 lettres des 20 pays dont la superficie est la plus
   grande?
3. Combien existe-t-il de monnaie officielle différente sur notre planète?
4. Combien y a-t-il de pays par continent?

### 5 : Requêtes sur le Canada

En utilisant le moins de commandes possibles, répondez aux questions suivantes.

1. Quels sont les fuseaux horaires possibles au Canada? Combien y en a-t-il?
2. Quels sont les lieux qui contiennent la chaîne `Longueuil`? Combien y en
   a-t-il?
3. Quelles sont les données qui ont été modifiées en 2018?
4. Quelles sont les longitudes minimale et maximale, ainsi que les latitudes
   minimale et maximale des lieux décrits dans le fichier `CA.txt`?
