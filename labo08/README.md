# Laboratoire 8 - Administration

## 1. SSH et Tmux (40 minutes)

### 1.1. Connexion aux serveurs de Labunix

- À l'aide de la commande `ssh`, connectez-vous à la machine
  `java.labunix.uqam.ca`.
- Quels sont les autres utilisateurs connecté, et de quelle machines
  viennent-ils ? Utilisez la commande `who`.
- Explorez sommairement le système de fichiers disponible dans votre répertoire
  personnel.
- Quel est le résultat de `ls /home/`? Croyez-vous qu'il soit sécuritaire
  d'avoir accès à cette information?
- À l'aide de la commande `cd`, essayez de visiter le répertoire d'un autre
  utilisateur. Que se passe-t-il?
- Consultez les permissions des répertoires contenus dans `/home`.
- Déconnectez-vous du serveur `java`.
- Connectez-vous maintenant à la machine `zeta.labunix.uqam.ca`. Est-ce que les
  mêmes fichiers sont disponibles dans votre répertoire personnel? Pourquoi?

### 1.2. Génération de clés

- Générez une paire de clés RSA privée/publique en lançant la commande
  interactive `ssh-keygen` sans argument et en suivant les instructions.
- Affichez le contenu des clés publique et privée sur la sortie standard à
  l'aide de la commande `cat`.
- À l'aide de la commande `ssh-copy-id`, transférez votre clé publique
  (`id_rsa.pub`) sur le serveur `java`.
- Confirmez qu'il est maintenant possible de vous connecter sur `java` sans
  devoir saisir votre mot de passe.

### 1.3. Tmux

En consultant les dernières diapositives du chapitre 7 (réseau),
familiarisez-vous avec le logiciel Tmux:

- Connectez-vous en SSH sur le serveur `java`.
- Puis lancez une nouvelle session avec `tmux`.
- Créez plusieurs panneaux avec `Ctrl-B %` et `Ctrl-B "`.
- Changez la disposition des panneaux avec `Ctrl-B o` et `Espace`.
- Affichez l'heure avec `Ctrl-B t`.
- Détachez-vous de la session avec `Ctrl-B d`.
- Créez une deuxième session en relançant `tmux` puis quittez-la avec
  `Ctrl-B d`.
- Entrez la commande `tmux ls` pour lister les sessions existantes. Repérez les
  deux sessions que vous venez de créer.
- Chargez la première session que vous aviez créée avec `tmux a -t <nom>` où
  `<nom>` est remplacé par le nom de la session. Renommez-la session courante
  avec un nom plus facile à retenir et vérifiez le changement avec `Ctrl-B s`.
- Lancez le script [`ecrit`](./ecrit) disponible dans ce dépôt, détachez-vous
  de la session et déconnectez-vous du serveur `java`.
- Reconnectez-vous sur Java et chargez à nouveau la session `tmux` dans
  laquelle vous aviez lancé le script. Vérifiez que le script a bien continué
  de tourner malgré votre déconnection.